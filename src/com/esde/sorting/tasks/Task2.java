package com.esde.sorting.tasks;

import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) {
        String[] array = {"bbb", "aa", "cccc", "qqqqqq", "rrrrrrr"};
        System.out.println("bubbleSorting: " + Arrays.toString(bubbleSorting(array)));
        System.out.println("insertionSorting: " + Arrays.toString(insertionSorting(array)));
        System.out.println("sectionSorting: " + Arrays.toString(sectionSorting(array)));
    }

    public static String[] bubbleSorting(String[] array) {

        boolean isSorted = false;
        String temp;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {

                String currentElement = array[i];
                String nextElement = array[i + 1];

                if (currentElement.compareTo(nextElement) > 0) {
                    isSorted = false;
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
        return array;
    }


    public static String[] insertionSorting(String[] array) {
        for (int i = 1; i < array.length; i++) {

            String currentElement = array[i];
            String previousElement = array[i  - 1];
            int previousKey = i - 1;

            while (previousKey >= 0 && currentElement.compareTo(previousElement) < 0) {
                array[previousKey + 1] = array[previousKey];
                array[previousKey] = currentElement;
                previousKey--;
            }
        }
        return array;
    }

    public static String[] sectionSorting(String[] array) {
        int pos;
        String temp;

        for (int i = 0; i < array.length; i++) {
            pos = i;
            for (int j = i + 1; j < array.length; j++) {
                String firstElement = array[j];
                String secondElement = array[pos];

                if (firstElement.compareTo(secondElement) < 0) {
                    pos = j;
                }
            }

            temp = array[pos];
            array[pos] = array[i];
            array[i] = temp;
        }
        return array;
    }
}
