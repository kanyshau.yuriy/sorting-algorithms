package com.esde.sorting.tasks;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        char[] array = {'v', 'c', 'm', 'a', 'x', 'z', 'p', 'w', 'l'};

        System.out.println("bubbleSorting: " + Arrays.toString(bubbleSorting(array)));
        System.out.println("insertionSorting: " + Arrays.toString(insertionSorting(array)));
        System.out.println("sectionSorting: " + Arrays.toString(sectionSorting(array)));
    }

    public static char[] bubbleSorting(char[] array) {

        boolean isSorted = false;
        char temp;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {

                if (array[i] > array[i + 1]) {
                    isSorted = false;

                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
        return array;
    }


    public static char[] insertionSorting(char[] array) {
        for (int i = 1; i < array.length; i++) {

            char currentElement = array[i];
            int previousKey = i - 1;

            while (previousKey >= 0 && array[previousKey] > currentElement) {
                array[previousKey + 1] = array[previousKey];
                array[previousKey] = currentElement;
                previousKey--;
            }
        }
        return array;
    }

    public static char[] sectionSorting(char[] array) {
        int pos;
        char temp;
        for (int i = 0; i < array.length; i++) {
            pos = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[pos]) {
                    pos = j;
                }
            }

            temp = array[pos];
            array[pos] = array[i];
            array[i] = temp;
        }
        return array;
    }
}
